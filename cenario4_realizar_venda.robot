*** Settings ***
Library     Selenium2Library
Resource    resources/common.robot
Test Setup  Iniciar Navegador e Logar
Test Teardown   Close all Browsers

*** Variables ***
${paginaIncluirVenda}     http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda
${msgSucesso}   class=alert-success

*** Test Cases ***
1 - Realizar Venda
    Go To   ${paginaIncluirVenda}
    Select From List  id=cliente    Cliente Teste
    Input Text  id=valorTransacao    200
    Click Button    id=botaoSalvar
    Element Should Be Visible  ${msgSucesso}