*** Settings ***
Library     Selenium2Library
Resource    resources/common.robot
Test Setup  Iniciar Navegador
Test Teardown   Close all Browsers

*** Variables ***
${usernameErrado}   aaa
${senhaErrada}  aaa

${usernameCorreto}   admin
${senhaCorreta}  admin

${linkDaAplicacao}  http://provaqa.marketpay.com.br:9084/desafioqa/inicio

*** Test Cases ***

1 - Verificar login com campos "Login" e "Password" vazios
    Click Button    class=btn-primary
    Page Should Contain     Credenciais inválidas

2 - Verificar login com campo "Login" preenchido e campo "Password" vazio.
    Input Text      name=username       admin
    Click Button    class=btn-primary
    Page Should Contain     Credenciais inválidas

3 - Verificar login com campo "Password" preenchido e campo "Vazio" vazio.
    Input Text      name=password       admin
    Click Button    class=btn-primary
    Page Should Contain     Credenciais inválidas

4 - Verificar login com credenciais de usuário inválidas.
    Input Text      name=username       ${usernameErrado}
    Input Text      name=password       ${senhaErrada}
    Click Button    class=btn-primary
    Page Should Contain     Credenciais inválidas

5 - Verificar o acesso ao link da página principal diretamente sem logar
    Go To   ${linkDaAplicacao}
    Page Should Contain Element     id=login-form

6 - Verificar login com credenciais corretas
    Logar na aplicacao  ${usernameCorreto}  ${senhaCorreta}
    Page Should Contain     Bem vindo ao Desafio