*** Settings ***
Library     Selenium2Library
Resource    resources/common.robot
Test Setup  Iniciar Navegador e Logar
Test Teardown   Close all Browsers

*** Variables ***
${paginaListarVenda}     http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda
${vendaPesquisada}      //*[@id="formListarTransacao"]/div/div/div/table/tbody/tr[1]/td[1]
${btnPesquisar}   //*[@id="formListarTransacao"]/div/div/fieldset[2]/div/div/div/input

*** Test Cases ***
1 - Listar Venda
    Go To   ${paginaListarVenda}
    Select From List  id=cliente    Cliente Teste
    Click Button    ${btnPesquisar}
    Element Text Should Be     ${vendaPesquisada}     Cliente Teste