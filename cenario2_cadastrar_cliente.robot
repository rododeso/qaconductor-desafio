*** Settings ***
Library     Selenium2Library
Resource    resources/common.robot
Test Setup  Iniciar Navegador e Logar
Test Teardown   Close all Browsers

*** Variables ***
${paginaIncluirCliente}     http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente
${botaoLimpar}    id=botaoLimpar
${botaoSalvar}    id=botaoSalvar
${botaoCancelar}  class=btn btn-danger

${inputNome}                id=nome
${inputCpf}                 id=cpf
${inputAtivo}               id=status
${inputSaldoDisponivel}     id=saldoCliente

${msgSucesso}   class=alert-success

*** Test Cases ***
1 - Acessar a tela de Cadastro de Cliente
    Go To   ${paginaIncluirCliente}
    Page Should Contain    Incluir Cliente

2 - Tentar salvar um cliente com os campos vazios
    Go To   ${paginaIncluirCliente}
    Click Button    ${botaoSalvar}
    Element Should Be Visible   class=help-block

7 - Salvar cliente válido
    Go To   ${paginaIncluirCliente}
    Input Text  ${inputNome}    Cliente Teste
    Input Text  ${inputCpf}     00000000000
    Input Text  ${inputSaldoDisponivel}     200
    Click Button    ${botaoSalvar}
    Element Should Be Visible  ${msgSucesso}
    