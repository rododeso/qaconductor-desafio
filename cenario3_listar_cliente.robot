*** Settings ***
Library     Selenium2Library
Resource    resources/common.robot
Test Setup  Iniciar Navegador e Logar
Test Teardown   Close all Browsers

*** Variables ***
${paginaListarCliente}     http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente
${inputPesquisa}    xpath=//*[@id="formListarCliente"]/div/fieldset/div/div/div[1]/input
${clienteEncontrado}    xpath=//*[@id="formListarCliente"]/div/div/table/tbody/tr[1]/td[1]

${btnPesquisar}             css=#formListarCliente > div > fieldset > div > div > div.col-sm-12.col-md-1 > input
${btnVisualizarCliente}     xpath=//*[@id="formListarCliente"]/div/div/table/tbody/tr[1]/td[5]/a[1]

*** Test Cases ***
1 - Consultar Cliente
    Go To   ${paginaListarCliente}
    Input Text  ${inputPesquisa}    Cliente Teste
    Click Button    ${btnPesquisar}
    Wait Until Page Contains Element    id=formListarCliente
    Element Text Should Be     ${clienteEncontrado}     Cliente Teste

2 - Visualizar Cliente
    Go To   ${paginaListarCliente}
    Input Text  ${inputPesquisa}    Cliente Teste
    Click Button    ${btnPesquisar}
    Wait Until Page Contains Element    id=formListarCliente
    Element Text Should Be     ${clienteEncontrado}     Cliente Teste
    Click Element    ${btnVisualizarCliente}
    Page Should Contain     Visualizar Cliente